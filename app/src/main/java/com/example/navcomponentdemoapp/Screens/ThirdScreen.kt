package com.example.navcomponentdemoapp.Screens

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.navcomponentdemoapp.HomeActivity
import com.example.navcomponentdemoapp.R
import kotlinx.android.synthetic.main.fragment_first_screen.view.*
import kotlinx.android.synthetic.main.fragment_first_screen.view.textView4
import kotlinx.android.synthetic.main.fragment_third_screen.view.*


class ThirdScreen : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_third_screen, container, false)
        view.textView4.setOnClickListener {
            activity?.startActivity(Intent(activity?.applicationContext,HomeActivity ::class.java))
            activity?.finish()
        }
        return view
    }




}