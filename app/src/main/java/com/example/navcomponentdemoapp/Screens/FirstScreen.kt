package com.example.navcomponentdemoapp.Screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.navcomponentdemoapp.R
import kotlinx.android.synthetic.main.fragment_first_screen.view.*

class FirstScreen : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_first_screen, container, false)
        view.textView4.setOnClickListener {
            var viewpager=activity?.findViewById<ViewPager2>(R.id.viewPager2)
            viewpager?.currentItem=1
        }
        return view
    }

}