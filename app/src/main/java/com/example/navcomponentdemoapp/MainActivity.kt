package com.example.navcomponentdemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        navController=(supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment).navController

        Handler().postDelayed({
            navController.navigate(R.id.action_splashFragment_to_viewPagerFragment)
        },3000)
    }
}