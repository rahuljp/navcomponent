package com.example.navcomponentdemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_bnv.*

class BNV : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bnv)
        supportActionBar?.title="Bottom Navigation View"

        var navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView2) as NavHostFragment
        var navController =navHostFragment.navController
        bottomNavigationView.setupWithNavController(navController)

        //setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))
    }

//    override fun onSupportNavigateUp(): Boolean {
//        var navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView2) as NavHostFragment
//        var navController =navHostFragment.navController
//        return navController.navigateUp() || super.onSupportNavigateUp()
//    }
}