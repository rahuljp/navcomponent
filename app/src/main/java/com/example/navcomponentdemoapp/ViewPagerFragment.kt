package com.example.navcomponentdemoapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.example.navcomponentdemoapp.Adapters.ScreenViewPagerAdapter
import com.example.navcomponentdemoapp.Screens.FirstScreen
import com.example.navcomponentdemoapp.Screens.SecondScreen
import com.example.navcomponentdemoapp.Screens.ThirdScreen
import kotlinx.android.synthetic.main.fragment_view_pager.view.*


class ViewPagerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_view_pager, container, false)
        var list= arrayListOf<Fragment>(
            FirstScreen(),
            SecondScreen(),
            ThirdScreen()
        )
        view.viewPager2.adapter=ScreenViewPagerAdapter(requireActivity().supportFragmentManager,list,lifecycle)
        return view
    }

}