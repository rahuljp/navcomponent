package com.example.navcomponentdemoapp.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.navcomponentdemoapp.ViewPagerFragment

class ScreenViewPagerAdapter( fm : FragmentManager,  list : ArrayList<Fragment>,  lifecycle: Lifecycle) : FragmentStateAdapter(fm,lifecycle){
    var fragment_list=list

    override fun getItemCount(): Int {
        return fragment_list.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragment_list[position]
    }


}